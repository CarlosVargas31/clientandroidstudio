package com.example.apppruebafirestore;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.apppruebafirestore.model.Lot;
import com.example.apppruebafirestore.model.LotAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FindLotActivity extends AppCompatActivity {

    private final String COLLECTION_KEY = "lots";

    private static final String TAG = FindLotActivity.class.getSimpleName();

    private TextView mHeaderView;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_lot);

        mHeaderView = (TextView) findViewById(R.id.findHeader);
        mHeaderView.setText("Find Lots");
        db = FirebaseFirestore.getInstance();
    }

    public void btnFindById_Click(View view) {
        EditText findText = (EditText) findViewById(R.id.EditTextFind);
        try {
            if (findText.getText() != null || findText.getText().equals("")) {
                DocumentReference docRef = db.collection(COLLECTION_KEY).document(findText.getText().toString());
                System.out.println(docRef.getPath());
                if (docRef != null) {
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            List<Lot> lots = new ArrayList<>();
                            Lot lot = documentSnapshot.toObject(Lot.class);
                            lots.add(lot);
                            fillList(lots);
                        }
                    });
                } else {
                    Toast.makeText(FindLotActivity.this,"Estudiante NO encontrado!",Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btnFinByParameter_Click(View view) {
        EditText findText = (EditText) findViewById(R.id.EditTextFind);

        try {
            if (findText.getText() != null || findText.getText().equals("")) {
                db.collection(COLLECTION_KEY)
                        .whereGreaterThan("stock", Integer.parseInt(findText.getText().toString()))
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                List<Lot> lots = new ArrayList<>();
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());
                                        Lot lot = document.toObject(Lot.class);
                                        lots.add(lot);
                                    }
                                    fillList(lots);
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fillList(List<Lot> lots) {
        ListView lotListView = (ListView) findViewById(R.id.lotList);
        LotAdapter lotAdapter = new LotAdapter(this, lots);
        lotListView.setAdapter(lotAdapter);
        for (Lot lot1: lots) {
            System.out.println(lot1);
        }
    }

}
