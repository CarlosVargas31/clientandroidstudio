package com.example.apppruebafirestore;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.apppruebafirestore.model.Warehouse;
import com.example.apppruebafirestore.model.WarehouseAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class WarehouseActivity extends AppCompatActivity {

    private final String COLLECTION_KEY = "warehouses";

    private static final String TAG = WarehouseActivity.class.getSimpleName();

    private TextView mHeaderView;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse);

        mHeaderView = (TextView) findViewById(R.id.warehouseHeader);
        mHeaderView.setText("Available Warehouses");
        db = FirebaseFirestore.getInstance();
        loadData();
    }

    public void loadData() {
        db.collection(COLLECTION_KEY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<Warehouse> warehouses = new ArrayList<>();
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Warehouse warehouse = document.toObject(Warehouse.class);
                                warehouses.add(warehouse);
                            }
                            fillList(warehouses);
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void fillList(List<Warehouse> warehouses) {
        ListView productListView = (ListView) findViewById(R.id.warehouseList);
        WarehouseAdapter warehouseAdapter = new WarehouseAdapter(this, warehouses);
        productListView.setAdapter(warehouseAdapter);
    }

}
