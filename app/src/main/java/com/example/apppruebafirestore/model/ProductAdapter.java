package com.example.apppruebafirestore.model;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.apppruebafirestore.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class ProductAdapter extends ArrayAdapter<Product> {

    public ProductAdapter(Context context, List<Product> object) {
        super( context,0, object);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView =  ((Activity)getContext()).getLayoutInflater().inflate(R.layout.item_product,parent,false);
        }

        TextView codeTextView = (TextView) convertView.findViewById(R.id.product_code);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.product_name);
        TextView priceTextView = (TextView) convertView.findViewById(R.id.product_price);

        Product product = getItem(position);

        codeTextView.setText(String.valueOf(product.getCode()));
        nameTextView.setText(product.getName());
        priceTextView.setText(String.valueOf(product.getPrice()));

        return convertView;
    }

}
