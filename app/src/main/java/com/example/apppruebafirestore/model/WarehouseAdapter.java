package com.example.apppruebafirestore.model;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.apppruebafirestore.R;

import java.util.List;

public class WarehouseAdapter extends ArrayAdapter<Warehouse> {

    public WarehouseAdapter(Context context, List<Warehouse> object) {
        super( context,0, object);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView =  ((Activity)getContext()).getLayoutInflater().inflate(R.layout.item_warehouse,parent,false);
        }

        TextView codeTextView = (TextView) convertView.findViewById(R.id.warehouse_code);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.warehouse_name);
        TextView addressTextView = (TextView) convertView.findViewById(R.id.warehouse_address);

        Warehouse warehouse = getItem(position);

        codeTextView.setText(String.valueOf(warehouse.getId()));
        nameTextView.setText(warehouse.getName());
        addressTextView.setText(warehouse.getAddress());

        return convertView;
    }

}
