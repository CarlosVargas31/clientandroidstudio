package com.example.apppruebafirestore.model;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.apppruebafirestore.R;

import java.util.List;

public class LotAdapter extends ArrayAdapter<Lot> {

    public LotAdapter(Context context, List<Lot> object) {
        super( context,0, object);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView =  ((Activity)getContext()).getLayoutInflater().inflate(R.layout.item_lot,parent,false);
        }

        TextView idTextView = (TextView) convertView.findViewById(R.id.lot_id);
        TextView productTextView = (TextView) convertView.findViewById(R.id.lot_product);
        TextView warehouseTextView = (TextView) convertView.findViewById(R.id.lot_warehouse);
        TextView stockTextView = (TextView) convertView.findViewById(R.id.lot_stock);

        Lot lot = getItem(position);

        idTextView.setText(String.valueOf(lot.getId()));
        productTextView.setText(lot.getProduct().getName());
        warehouseTextView.setText(lot.getWarehouse().getName());
        stockTextView.setText(String.valueOf(lot.getStock()));

        return convertView;
    }

}
