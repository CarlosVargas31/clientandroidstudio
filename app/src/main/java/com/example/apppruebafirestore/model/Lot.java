package com.example.apppruebafirestore.model;

public class Lot {

    private int id;

    private Product product;

    private Warehouse warehouse;

    private int stock;

    public Lot() { }

    public Lot(int id, Product product, Warehouse warehouse, int stock) {
        this.id = id;
        this.product = product;
        this.warehouse = warehouse;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "id=" + id +
                ", product=" + product +
                ", warehouse=" + warehouse +
                ", stock=" + stock +
                '}';
    }

}
