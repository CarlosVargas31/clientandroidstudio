package com.example.apppruebafirestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apppruebafirestore.model.Product;
import com.example.apppruebafirestore.model.ProductAdapter;
import com.example.apppruebafirestore.model.Warehouse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String COLLECTION_KEY = "products";

    private static final String TAG = MainActivity.class.getSimpleName();

    //Views
    private TextView mHeaderView;

    //Firebase
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mHeaderView = (TextView) findViewById(R.id.menuHeader);
        mHeaderView.setText("Select option");
        db = FirebaseFirestore.getInstance();
    }

    public void btnAdicionar_Click(View view) {
        Product product = new Product(222, "Carne", 1800);

        db.collection("products")
                .document(String.valueOf(product.getCode()))
                .set(product).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(MainActivity.this,"Estudiante adicionado!",Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(MainActivity.this,"Estudiante NO adicionado!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void btnProduct_Click(View view) {
        Intent intent = new Intent(this, ProductActivity.class);
        startActivity(intent);
    }

    public void btnWarehouse_Click(View view) {
        Intent intent = new Intent(this, WarehouseActivity.class);
        startActivity(intent);
    }

    public void btnLot_Click(View view) {
        Intent intent = new Intent(this, LotActivity.class);
        startActivity(intent);
    }

}