package com.example.apppruebafirestore;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.apppruebafirestore.model.Product;
import com.example.apppruebafirestore.model.ProductAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class ProductActivity extends AppCompatActivity {

    private final String COLLECTION_KEY = "products";

    private static final String TAG = ProductActivity.class.getSimpleName();

    private TextView mHeaderView;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        mHeaderView = (TextView) findViewById(R.id.productHeader);
        mHeaderView.setText("Available Products");
        db = FirebaseFirestore.getInstance();
        loadData();
    }

    public void loadData() {
        db.collection(COLLECTION_KEY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<Product> products = new ArrayList<>();
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Product product = document.toObject(Product.class);
                                products.add(product);
                            }
                            fillList(products);
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void fillList(List<Product> products) {
        ListView productListView = (ListView) findViewById(R.id.productList);
        ProductAdapter productAdapter = new ProductAdapter(this, products);
        productListView.setAdapter(productAdapter);
    }

}
