package com.example.apppruebafirestore;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.apppruebafirestore.model.Lot;
import com.example.apppruebafirestore.model.LotAdapter;
import com.example.apppruebafirestore.model.Product;
import com.example.apppruebafirestore.model.Warehouse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class LotActivity extends AppCompatActivity {

    private final String COLLECTION_KEY = "lots";

    private static final String TAG = LotActivity.class.getSimpleName();

    private TextView mHeaderView;

    private FirebaseFirestore db;

    private List<Lot> lots;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lot);

        mHeaderView = (TextView) findViewById(R.id.lotHeader);
        mHeaderView.setText("Available Lots");
        db = FirebaseFirestore.getInstance();
        this.lots = new ArrayList<>();
        loadData();
    }

    public void loadData() {
        db.collection(COLLECTION_KEY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        List<Lot> lots = new ArrayList<>();
                        if (task.isSuccessful()) {
                            for (final DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Lot lot = document.toObject(Lot.class);
                                lots.add(lot);
                                /*DocumentReference refProduct = document.getDocumentReference("product");

                                refProduct.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        final Product product = documentSnapshot.toObject(Product.class);

                                        DocumentReference refWarehouse = document.getDocumentReference("warehouse");

                                        refWarehouse.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                List<Lot> lots = new ArrayList<>();
                                                Warehouse warehouse = documentSnapshot.toObject(Warehouse.class);
                                                int id = document.get("id").hashCode();
                                                int stock = document.get("stock").hashCode();
                                                Lot lot = new Lot(id, product, warehouse, stock);
                                                lots.add(lot);
                                                if(task.getResult().size() == lots.size()) {
                                                    System.out.println("Entro");
                                                    fillList(lots);
                                                }
                                                System.out.println(lots.size());
                                            }
                                        });
                                    }
                                });*/
                            }
                            fillList(lots);
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void fillList(List<Lot> lots) {
        ListView lotListView = (ListView) findViewById(R.id.lotList);
        LotAdapter lotAdapter = new LotAdapter(this, lots);
        lotListView.setAdapter(lotAdapter);
        for (Lot lot1: lots) {
            System.out.println(lot1);
        }
    }

    public void btnFind_Click(View view) {
        Intent intent = new Intent(this, FindLotActivity.class);
        startActivity(intent);
    }

}
